using System.Text.Json;
using FastEndpoints;
using FastEndpoints.Swagger;
using PixelPlayInc_csharp.Common;
using PixelPlayInc_csharp.Licenses;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((host, log) =>
{
    if (host.HostingEnvironment.IsProduction())
        log.MinimumLevel.Information();

    log.WriteTo.Console();
});

builder.Services.AddMediatR(cfg =>
{
    cfg.RegisterServicesFromAssemblyContaining<Program>();
});
builder.Services.AddFastEndpoints(o =>
{
    o.IncludeAbstractValidators = true;
});
builder.Services.SwaggerDocument(o =>
{
    o.DocumentSettings = s =>
    {
        s.Title = "PixelPlayInc Api";
        s.Version = "v1";
    };
});

builder.Services.AddSingleton<IDateTimeProvider, DateTimeProvider>();

builder.Services.RegisterLicenseModule();

var app = builder.Build();

app.UseFastEndpoints(c =>
{
    c.Serializer.Options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    c.Endpoints.RoutePrefix = "api";
});
app.UseSwaggerGen();

app.Run();

public partial class Program {}