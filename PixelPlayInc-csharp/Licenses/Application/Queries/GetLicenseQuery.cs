namespace PixelPlayInc_csharp.Licenses.Application.Queries;

public record GetLicenseQuery(Guid LicenseId);