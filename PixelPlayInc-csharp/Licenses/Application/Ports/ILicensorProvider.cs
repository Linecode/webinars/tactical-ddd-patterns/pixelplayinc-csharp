using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Application.Ports;

public interface ILicensorProvider
{
    Task<Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData, CancellationToken ct);
}