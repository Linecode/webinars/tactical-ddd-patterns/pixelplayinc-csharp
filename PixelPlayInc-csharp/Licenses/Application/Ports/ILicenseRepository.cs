using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licenses;

namespace PixelPlayInc_csharp.Licenses.Application.Ports;

public interface ILicenseRepository
{
    Task Add(License license, CancellationToken cancellationToken = default);
    
    Task Update(License license, CancellationToken cancellationToken = default);
    
    Task<License?> Get(Guid id, CancellationToken cancellationToken = default);

    Task<LicenseReadModel> GetReadModel(Guid id, ILicensorProvider licensorProvider,
        CancellationToken cancellationToken = default);
}