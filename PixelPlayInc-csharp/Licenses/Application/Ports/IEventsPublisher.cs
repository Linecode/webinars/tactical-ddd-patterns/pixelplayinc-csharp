using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Application.Ports;

public interface IEventsPublisher
{
    Task Publish<T>(IEnumerable<T> events, CancellationToken ct = default)
        where T : DomainEvent;

    Task Publish<T>(T @event, CancellationToken ct = default)
        where T : DomainEvent;
}