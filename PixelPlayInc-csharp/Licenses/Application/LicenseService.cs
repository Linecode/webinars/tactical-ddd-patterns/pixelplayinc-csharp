using PixelPlayInc_csharp.Common;
using PixelPlayInc_csharp.Licenses.Application.Commands;
using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Application;

public interface ILicenseService
{
    Task<Guid> Create(CreateLicenseCommand command, CancellationToken ct);
    Task Activate(ActivateLicenseCommand command, CancellationToken ct);
    Task ExpireLicense(ExpireLicenseCommand command, CancellationToken ct);
}

[ApplicationService]
public class LicenseService(
    ILicenseRepository repository, 
    ILicensorProvider licensorProvider,
    IDateTimeProvider dateTimeProvider,
    IEventsPublisher eventsPublisher,
    ILogger<LicenseService> logger) : ILicenseService
{
    public async Task<Guid> Create(CreateLicenseCommand command, CancellationToken cancellationToken = default)
    {
        var (dateRange, decryptionKey, licensorIdOrData, region) = command;

        var licensor = await licensorProvider.GetOrCreate(licensorIdOrData, cancellationToken);
        var license = License.Create(dateRange, decryptionKey, licensor, region);
        
        await repository.Add(license, cancellationToken);

        return license.Id;
    }

    public async Task Activate(ActivateLicenseCommand command, CancellationToken cancellationToken = default)
    {
        var license = await repository.Get(command.LicenseId, cancellationToken);
        
        if (license is null)
            throw new Exception("License not found");
        
        var events = license.Activate(dateTimeProvider);
        
        await repository.Update(license, cancellationToken);
        
        await eventsPublisher.Publish(events, cancellationToken);
    }

    public async Task ExpireLicense(ExpireLicenseCommand command, CancellationToken ct)
    {
        var license = await repository.Get(command.LicenseId, ct);
        
        if (license is null)
            throw new Exception("License not found");
        
        var events = license.Expire(dateTimeProvider);
        
        await repository.Update(license, ct);
        
        await eventsPublisher.Publish(events, ct);
    }
}