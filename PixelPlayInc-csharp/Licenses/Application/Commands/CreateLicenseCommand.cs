using PixelPlayInc_csharp.Common;
using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Application.Commands;

public record CreateLicenseCommand(
    DateRange DateRange, 
    DecryptionKey DecryptionKey, 
    LicensorIdOrData LicensorIdOrData, 
    RegionIdentifier Region);