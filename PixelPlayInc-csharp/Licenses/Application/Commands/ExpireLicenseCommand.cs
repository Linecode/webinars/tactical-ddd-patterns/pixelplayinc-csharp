namespace PixelPlayInc_csharp.Licenses.Application.Commands;

public record ExpireLicenseCommand(Guid LicenseId);