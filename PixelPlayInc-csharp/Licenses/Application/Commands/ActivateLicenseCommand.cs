namespace PixelPlayInc_csharp.Licenses.Application.Commands;

public record ActivateLicenseCommand(Guid LicenseId);