using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Application.Queries;
using PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licenses;
using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Application;

public interface ILicenseQueryService
{
    Task<LicenseReadModel> Get(GetLicenseQuery query, CancellationToken ct);
}

[ApplicationService]
public class LicenseQueryService(ILicenseRepository repository, ILicensorProvider licensorProvider) : ILicenseQueryService
{
    public async Task<LicenseReadModel> Get(GetLicenseQuery query, CancellationToken ct = default)
    {
        return await repository.GetReadModel(query.LicenseId, licensorProvider, ct);
    }
}