using PixelPlayInc_csharp.Licenses.Application;
using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Infrastructure.IntegrationEvents;
using PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licenses;
using PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licensors;

namespace PixelPlayInc_csharp.Licenses;

public abstract class LicenseModule
{
}

public static class LicenseModuleExtensions
{
    public static IServiceCollection RegisterLicenseModule(this IServiceCollection services)
    {
        services.AddScoped<ILicenseService, LicenseService>();
        services.AddScoped<ILicenseQueryService, LicenseQueryService>();
        services.AddScoped<IEventsPublisher, NotificationPublisher>();
        
        services.AddSingleton<ILicenseRepository, LicenseRepository>();
        services.AddSingleton<ILicensorProvider, LicensorProvider>();
        
        return services;
    }
}