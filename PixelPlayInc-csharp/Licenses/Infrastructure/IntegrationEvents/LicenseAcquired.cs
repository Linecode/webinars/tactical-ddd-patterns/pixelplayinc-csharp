using MediatR;
using PixelPlayInc_csharp.Common;

namespace PixelPlayInc_csharp.Licenses.Infrastructure.IntegrationEvents;

public record LicenseAcquired(Guid LicenseId, RegionIdentifier Region) : INotification;
public record LicenseExpired(Guid LicenseId) : INotification;