using MediatR;
using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Infrastructure.IntegrationEvents;

public class NotificationPublisher(IPublisher publisher) : IEventsPublisher
{
    public async Task Publish<T>(T @event, CancellationToken ct = default)
        where T : DomainEvent
    {
        var notification = MapFromDomainEvent(@event);

        if (notification is not null)
            await publisher.Publish(notification, ct);
    }

    public async Task Publish<T>(IEnumerable<T> events, CancellationToken ct = default)
        where T : DomainEvent
    {
        foreach (var @event in events)
        {
            await Publish(@event, ct);
        }
    }

    private static INotification? MapFromDomainEvent(DomainEvent domainEvent)
    {
        return domainEvent switch
        {
            License.Events.LicenseActivated @event => new LicenseAcquired(@event.LicenseId, @event.Region),
            License.Events.LicenseExpired @event => new LicenseExpired(@event.LicenseId),
            _ => null
        };
    }
}