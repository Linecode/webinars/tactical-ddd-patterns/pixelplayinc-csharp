using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licensors;

public class LicensorProvider : Dictionary<Guid, Licensor>, ILicensorProvider
{
    public Task<Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData, CancellationToken ct)
    {
        if (licensorIdOrData.LicensorId.HasValue)
        {
            if (ContainsKey(licensorIdOrData.LicensorId.Value))
            {
                return Task.FromResult(this[licensorIdOrData.LicensorId.Value]);
            }
        }

        var id = Guid.NewGuid();
        var licensor = new Licensor(id, licensorIdOrData.Name!, licensorIdOrData.Address!);
        
        Add(licensor.Id, licensor);

        return Task.FromResult(licensor);
    }
}