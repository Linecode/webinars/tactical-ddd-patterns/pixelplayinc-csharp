using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licenses;

public record LicenseReadModel(
    Guid Id,
    DateTime From,
    DateTime To,
    string DecryptionKey,
    int Territory,
    string Status,
    Licensor Licensor)
{
    public static LicenseReadModel Map(License license, Licensor licensor)
    {
        return new LicenseReadModel(
            license.Id,
            license.DateRange.From,
            license.DateRange.To,
            license.DecryptionKey.Value,
            license.Region.Value,
            license.State.ToString(),
            licensor);
    }
};