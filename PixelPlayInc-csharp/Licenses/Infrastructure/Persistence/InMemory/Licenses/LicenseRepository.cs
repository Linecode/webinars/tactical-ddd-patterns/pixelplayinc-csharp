using PixelPlayInc_csharp.Licenses.Application.Ports;
using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Infrastructure.Persistence.InMemory.Licenses;

public class LicenseRepository: Dictionary<Guid, License>, ILicenseRepository
{
    public Task Add(License license, CancellationToken cancellationToken = default)
    {
        Add(license.Id, license);
        
        return Task.CompletedTask;
    }

    public Task Update(License license, CancellationToken cancellationToken = default)
    {
        this[license.Id] = license;
        
        return Task.CompletedTask;
    }

    public Task<License?> Get(Guid id, CancellationToken cancellationToken = default)
    {
        var exists = TryGetValue(id, out var license);
        
        return Task.FromResult(exists? license : null);
    }

    public async Task<LicenseReadModel> GetReadModel(Guid id, ILicensorProvider licensorProvider, CancellationToken cancellationToken = default)
    {
        var exists = TryGetValue(id, out var license);

        if (!exists)
            throw new Exception("License not found");
        
        var licensor = await licensorProvider.GetOrCreate(new LicensorIdOrData(license!.LicensorId, null, null), cancellationToken);
        
        return LicenseReadModel.Map(license, licensor);
    }
}