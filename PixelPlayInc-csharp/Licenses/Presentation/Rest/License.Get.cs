using FastEndpoints;
using PixelPlayInc_csharp.Licenses.Application;
using PixelPlayInc_csharp.Licenses.Application.Queries;

namespace PixelPlayInc_csharp.Licenses.Presentation.Rest;

public class LicenseGet(ILicenseQueryService service) : EndpointWithoutRequest
{
    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");
        
        var license = await service.Get(new GetLicenseQuery(id), ct);

        await SendAsync(license, 200, ct);
    }

    public override void Configure()
    {
        Get("/license/{Id}");
        AllowAnonymous();
    }
}