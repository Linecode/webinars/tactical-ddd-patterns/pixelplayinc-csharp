using FastEndpoints;
using PixelPlayInc_csharp.Licenses.Application;
using PixelPlayInc_csharp.Licenses.Application.Commands;

namespace PixelPlayInc_csharp.Licenses.Presentation.Rest;

public class LicenseActivate(ILicenseService service) : EndpointWithoutRequest
{
    public override void Configure()
    {
        Post("/license/{Id}/activate");
        AllowAnonymous();
    }

    public override async Task HandleAsync(CancellationToken cancellationToken)
    {
        var id = Route<Guid>("Id");
        
        await service.Activate(new ActivateLicenseCommand(id), cancellationToken);

        await SendAsync("", 200, cancellationToken);
    }
}