using FastEndpoints;
using FluentValidation;
using PixelPlayInc_csharp.Common;
using PixelPlayInc_csharp.Licenses.Application;
using PixelPlayInc_csharp.Licenses.Application.Commands;
using PixelPlayInc_csharp.Licenses.Domain;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;

namespace PixelPlayInc_csharp.Licenses.Presentation.Rest;

public class LicenseCreate(ILicenseService service) : Endpoint<CreateLicenseResource>
{
    public override void Configure()
    {
        Post("/license");
        AllowAnonymous();
        Summary(new EndpointSummary());
    }
    
    public override async Task HandleAsync(CreateLicenseResource request, CancellationToken cancellationToken)
    {
        var (from, to, decryptionKey, licensor, territory) = request;

        var region = RegionIdentifier.Parse(territory);
        var dateRange = DateRange.New(from, to);
        var key = DecryptionKey.Parse(decryptionKey);
        var licensorIdOrData = new LicensorIdOrData(licensor.Id, licensor.Address, licensor.Name);
        
        var id = await service.Create(new CreateLicenseCommand(dateRange, key, licensorIdOrData, region), cancellationToken);
        
        HttpContext.Response.Headers.Add("Location", $"api/license/{id}");
        await SendAsync("", 201, cancellationToken);
    }

    private class EndpointSummary : Summary<LicenseCreate>
    {
        public EndpointSummary()
        {
            Summary = "Some summary";
            Description = "Some description";
        }
    }
}

public record CreateLicenseResource(
    DateTime From,
    DateTime To,
    string DecryptionKey,
    LicensorResource Licensor,
    int Territory)
{
    public class CreateLicenseValidator : AbstractValidator<CreateLicenseResource>
    {
        public CreateLicenseValidator()
        {
            // Data Correctness Validation
            RuleFor(x => x.From).NotEqual(DateTime.MinValue);
            RuleFor(x => x.To).NotEqual(DateTime.MinValue);
            RuleFor(x => x.DecryptionKey)
                .NotEmpty();
            RuleFor(x => x.Territory)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(999);
            RuleFor(x => x.Licensor).SetValidator(new LicensorResource.LicensorValidator());
        }
        
        private bool GuidValidator(string value) => Guid.TryParse(value, out _);
    }
}

public record LicensorResource(Guid? Id, string? Name, string? Address)
{
    public class LicensorValidator : AbstractValidator<LicensorResource>
    {
        public LicensorValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                    .When(x => string.IsNullOrEmpty(x.Name) && string.IsNullOrEmpty(x.Address));
            RuleFor(x => x.Name)
                .NotEmpty()
                    .When(x => x.Id == null || x.Id == Guid.Empty);
            RuleFor(x => x.Address)
                .NotEmpty()
                    .When(x => x.Id == null || x.Id == Guid.Empty);;
        }
    }
}