using FastEndpoints;
using PixelPlayInc_csharp.Licenses.Application;
using PixelPlayInc_csharp.Licenses.Application.Commands;

namespace PixelPlayInc_csharp.Licenses.Presentation.Rest;

public class LicenseExpire(ILicenseService service) : EndpointWithoutRequest
{
    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");
        
        await service.ExpireLicense(new ExpireLicenseCommand(id), ct);
        
        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/license/{Id}/expire");
        AllowAnonymous();
    }
}