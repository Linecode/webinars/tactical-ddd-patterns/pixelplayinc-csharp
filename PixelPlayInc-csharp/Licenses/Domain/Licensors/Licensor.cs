namespace PixelPlayInc_csharp.Licenses.Domain.Licensors;

public class Licensor(Guid id, string name, string address)
{
    public Guid Id { get; private set; } = id;
    public string Name { get; private set; } = name;
    public string Address { get; private set; } = address;
}