using EnsureThat;
using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Domain;

public class DecryptionKey : ValueObject<DecryptionKey>
{
    public string Value { get; private set; } = string.Empty;
    
    private DecryptionKey(string value)
    {
        Value = value;
    }

    public static DecryptionKey Parse(string value)
    {
        Ensure.That(value).HasLength(40);
        Ensure.That(value).StartsWith("key");
        Ensure.That(Guid.TryParse(value.Split("key:").Last(), out var id)).IsTrue();
        
        return new DecryptionKey(value);
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}