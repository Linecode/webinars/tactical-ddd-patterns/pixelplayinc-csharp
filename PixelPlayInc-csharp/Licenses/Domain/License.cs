using PixelPlayInc_csharp.Common;
using PixelPlayInc_csharp.Licenses.Domain.Licensors;
using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Licenses.Domain;

public sealed class License : Entity<Guid>, AggregateRoot<Guid>
{
    public enum States { Provisioned, Active, Expired }

    public States State { get; private set; } = States.Provisioned;
    public DateRange DateRange { get; private set; } = null!;
    public Guid LicensorId { get; private set; } = Guid.Empty;
    public RegionIdentifier Region { get; private set; } = null!;
    public DecryptionKey DecryptionKey { get; private set; } = null!;

    [Obsolete("Only for ORM", true)]
    private License() { }
    
    private License(Guid id) { Id = id; }

    public IEnumerable<DomainEvent> Activate(IDateTimeProvider provider)
    {
        if (State != States.Provisioned)
            throw new Exception("License already active");
        
        if (!DateRange.IsWithinRange(provider.GetUtcNow()))
            throw new Exception("License can't be activated yet");
        
        var @event = new Events.LicenseActivated(Guid.NewGuid(), Id, Region);
        
        Apply(@event);
        return new[] { @event };
    }
    
    private void Apply(Events.LicenseActivated @event)
    {
        State = States.Active;
    }
    
    public IEnumerable<DomainEvent> Expire(IDateTimeProvider dateTimeProvider)
    {
        // probably we should check To Date here but we leave it without check just to simulate expire at any time
        var @event = new Events.LicenseExpired(Guid.NewGuid(), Id);
        
        Apply(@event);

        return new[] { @event };
    }

    private void Apply(Events.LicenseExpired @event)
    {
        State = States.Expired;
    }

    private void Apply(Events.LicenseCreated @event)
    {
        DateRange = @event.DateRange;
        LicensorId = @event.LicensorId;
        Region = @event.Region;
        DecryptionKey = DecryptionKey.Parse(@event.DecryptionKey);
    }

    public static License Create(DateRange dateRange, DecryptionKey decryptionKey, Licensor licensor,
        RegionIdentifier region)
    {
        var id = Guid.NewGuid();
        var @event = new Events.LicenseCreated(Guid.NewGuid(),id, dateRange, licensor.Id, decryptionKey.Value, region);

        var license = new License(id);
        license.Apply(@event);
        
        return license;
    }

    public static class Events
    {
        public record LicenseCreated(
            Guid EventId, 
            Guid LicenseId, 
            DateRange DateRange, 
            Guid LicensorId,
            string DecryptionKey, 
            RegionIdentifier Region) : DomainEvent;
        
        public record LicenseActivated(Guid EventId, Guid LicenseId, RegionIdentifier Region) : DomainEvent;
        public record LicenseExpired(Guid EventId, Guid LicenseId) : DomainEvent;
    }
}