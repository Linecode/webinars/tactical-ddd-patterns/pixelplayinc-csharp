using PixelPlayInc.Common.BuildingBlocks;

namespace PixelPlayInc_csharp.Common;

public sealed class DateRange : ValueObject<DateRange>
{
    public DateTime From { get; private set; }
    public DateTime To { get; private set; }

    private DateRange() { }
    
    public static DateRange New(DateTime from, DateTime? to = null)
    {
        var dateTo = to ?? DateTime.MaxValue;
        
        if (from > dateTo)
            throw new ArgumentOutOfRangeException(nameof(from),$"Date from is greater than date to {to}");

        return new DateRange
        {
            From = from,
            To = dateTo
        };
    }

    public override string ToString() => $"{From:s} - {To:s}";

    public bool IsWithinRange(DateTime date) => date >= From && date <= To;

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return From;
        yield return To;
    }
}
