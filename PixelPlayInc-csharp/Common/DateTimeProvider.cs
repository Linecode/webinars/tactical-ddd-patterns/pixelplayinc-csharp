namespace PixelPlayInc_csharp.Common;

public interface IDateTimeProvider
{
    DateTime GetUtcNow();
}

public class DateTimeProvider : IDateTimeProvider
{
    public DateTime GetUtcNow() => DateTime.UtcNow;
}
