using MediatR;
using PixelPlayInc_csharp.Licenses.Infrastructure.IntegrationEvents;

namespace PixelPlayInc_csharp.Library.Application.Listeners;

public class LicenseAcquiredListener(ILogger<LicenseAcquiredListener> logger) : INotificationHandler<LicenseAcquired>
{
    public Task Handle(LicenseAcquired notification, CancellationToken cancellationToken)
    {
        logger.LogInformation("[Library BC]: License Acquired: {NotificationLicenseId}", notification.LicenseId);
        
        return Task.CompletedTask;
    }
}